# kube-net-debug.sh binary

## General information

* **Author**: ldawert
* **Description**: Shell binary to spawn a network troubleshooting pod and grab an interactive shell session in it

## Usage

* Clone this repository
* Move, link or copy binary `kube-net-debug.sh` to a location from your `$PATH` variable (make sure it's executable)
* Use binary

```shell
# starting pod
$ kube-net-debug.sh
pod/tmp-shell-ldawert created
Waiting for pod to become ready
Starting interactive session in pod...
bash-5.1#

# deleting pod
$ kube-net-debug.sh
Pod tmp-shell-ldawert is already running.
Should it be deleted? [y|n]
Y
pod "tmp-shell-ldawert" deleted
Deletion completed. Exiting...
```
