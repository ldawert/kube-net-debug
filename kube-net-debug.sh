#!/bin/bash

# configuration parameters
USERNAME="${USER}"
PODNAME="tmp-shell-${USERNAME}"
PODTEMPLATE="apiVersion: v1
kind: Pod
metadata:
  labels:
    run: ${PODNAME}
  name: ${PODNAME}
spec:
  containers:
  - image: nicolaka/netshoot
    name: ${PODNAME}
    resources: {}
    command:
    - sleep
    - '100000'
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}"

# check if pod is already running and ask for deletion
if [ "$(kubectl get pods ${PODNAME})" ]
then
  echo "Pod ${PODNAME} is already running."
  echo "Should it be deleted | attached? Or cancel operation. [d|a|c]"
  read DELETE_FLAG
  if [ "${DELETE_FLAG}" == "d" ] || [ "${DELETE_FLAG}" == "D" ]
  then 
    kubectl delete pod "${PODNAME}"
    echo "Deletion completed. Exiting..."
    exit 0
  elif [ "${DELETE_FLAG}" == "a" ] || [ "${DELETE_FLAG}" == "A" ]
  then
    echo "Attaching to pod..."
    kubectl exec -ti "${PODNAME}" -- /bin/bash
  else
    echo "Aborting..."
    exit 0
  fi
fi

# run pod from template
echo "${PODTEMPLATE}" | kubectl apply -f -
echo "Waiting for pod to become ready"

# check if pod is in state "Running"
while true
do
  if [ "$(kubectl get pods ${PODNAME} -o jsonpath='{.status.phase}')" == "Running" ]
  then
    break
  fi
done

# interactively grab session in pod
echo "Starting interactive session in pod..."
kubectl exec -ti "${PODNAME}" -- /bin/bash

